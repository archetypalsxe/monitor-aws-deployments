#! /bin/bash

maxTasks=5
service=$2
if [ "$TESTING" == 1 ]; then
    ecrTimeout="1 1"
    taskTimeout="1 1"
    taskPollTimeout="1 1"
else
    ecrTimeout="15 20"
    taskTimeout="8 15"
    taskPollTimeout="20 30"
fi

if [ -z "$URLPATH" ]; then
    url=$(cat /usr/etc/awsMonitorSettings 2> /dev/null)
else
    url=$(cat $URLPATH 2> /dev/null)
fi

if [ -z "$url" ]
    then
        echo "URL must be configured (see readme)"
        exit 1
fi

function slackAlert {
    output=$(curl -s -X POST -H 'Content-type: application/json' --data "{\"text\":\"*$service*\n$1\"}" $url)
    if [ $output != "ok" ]
    then
        echo $output
    fi
}

function getDesiredServiceCount {
    result=$(aws ecs describe-services --services $2 --cluster $1 | jq ".services[0].desiredCount")
    echo $result
}

function getNewestEcr {
    result=$(aws ecr describe-images --repository-name $1 --query 'sort_by(imageDetails,& imagePushedAt)[-1].imageTags' $profile)
    echo ${result//[\"]/ }
}

function getNewestTaskVersion {
    echo $(aws ecs describe-task-definition --task-definition $1 | jq '.taskDefinition.revision')
}

function getTaskHashes {
    echo $(aws ecs list-tasks --cluster $1 --service-name $2 | jq ".taskArns")
}

function getTaskRevisionNumber {
    output=$(aws ecs describe-tasks --cluster $1 --tasks $2 | jq ".tasks[0].taskDefinitionArn" | cut -d ':' -f 7)
    echo "${output%?}"
}

function getNumTasksOnNewestRevision {
    count=0
    total=0
    output=$(getTaskHashes $1 $2 | jq -c '.[]' | while IFS= read object _; do
        object=${object//[\"]/}
        total=$((total + 1))
        revisionNumber=$(getTaskRevisionNumber $1 $object)
        if [ $3 -eq $revisionNumber ]
        then
            count=$(($count+1))
        fi
        echo $count $total
    done)
    echo $output | rev | cut -d ' ' -f 1,2 | rev
}

function loopUntilChanged {
    startingValue=$1
    newValue=$1
    commandToRun=$2
    retryAttempts=$3
    sleepTime=$4

    counter=1
    while [ "$counter" -le "$retryAttempts" ]; do
        newValue=`$commandToRun`
        if [ "$startingValue" != "$newValue" ]; then
            break
        fi
        counter=$(($counter + 1))
        sleep $sleepTime
    done

    if [ $counter -ge $retryAttempts ]
    then
        slackAlert "\`$commandToRun\` did not change within $retryAttempts attempts $sleepTime seconds apart. Value started as $startingValue and was $newValue at end"
        if [ "$TESTING" != 1 ]; then
            exit 1
        fi
    fi
}

if [ -z "$4" ]
    then
        echo "Please provide cluster name, repo name, task name, service name, service type (optional, example = fargate) and profile (optional)"
        exit 1;
else
    if [ -z "$6" ]
    then
        profile=""
        profileText="Profile not provided"
    else
        profile=" --profile=$6"
        profileText="$6"
    fi

    ecrVersion=$(getNewestEcr $2)
    taskVersion=$(getNewestTaskVersion $3)
    taskHashes=$(getTaskHashes $1 $4)
    output=$(getNumTasksOnNewestRevision $1 $4 $taskVersion)
    desiredCount=$(getDesiredServiceCount $1 $4)

    line1="Monitoring cluster: $1, repo: $2, task: $3, service: $4 and profile: $profileText"
    line2="\nStarting repo version: $ecrVersion"
    line3="\nStarting task version: $taskVersion"
    line4="\nDesired number of tasks: $desiredCount"
    slackAlert "$line1$line2$line3$line4"

    loopUntilChanged "$ecrVersion" "getNewestEcr $2" $ecrTimeout
    newEcrVersion=$(getNewestEcr $2)

    loopUntilChanged "$taskVersion" "getNewestTaskVersion $3" $taskTimeout
    newTaskVersion=$(getNewestTaskVersion $3)

    count=0
    newCounter=1
    while [ "$newCounter" -le "$maxTasks" ]; do
        loopUntilChanged "$taskHashes" "getTaskHashes $1 $4" $taskPollTimeout
        taskHashes=$(getTaskHashes $1 $4)
        output=$(getNumTasksOnNewestRevision $1 $4 $newTaskVersion)
        count=$(echo $output | cut -d " " -f 1)
        current=$(echo $output | cut -d " " -f 2)
        slackAlert "$count of $desiredCount tasks are moved to the newest version ($newTaskVersion). Currently running $current tasks"
        newCounter=$(($newCounter+1))
        if [ "$count" == "$desiredCount" ] && [ $current == $desiredCount ]; then
            break
        fi
    done

    if [ "$5" == "fargate" ]; then
        taskId=$(aws ecs list-tasks --cluster $1 --service $4 | jq ".taskArns[0]")
        # Removing quotes
        taskId=$(sed -e 's/^"//' -e 's/"$//' <<< "$taskId")
        networkInterface=$(aws ecs describe-tasks --cluster $1 --task $taskId | jq '.tasks[0].attachments[0].details[1].value')
        # Removing more quotes
        networkInterface=$(sed -e 's/^"//' -e 's/"$//' <<< "$networkInterface")
        publicIp=$(aws ec2 describe-network-interfaces --network-interface-ids $networkInterface | jq -r '.NetworkInterfaces[].PrivateIpAddresses[].Association.PublicDnsName')
        slackAlert "New Public IP: $publicIp"
    fi
fi
