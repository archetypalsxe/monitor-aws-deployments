# Monitor AWS Deployments
Tool for posting updates to Slack to follow AWS ECS deployments as they occur

# Dependencies
* curl
* jq

# Setup
Create `.url` file that has the Slack Webhook to use in it (and nothing else)

# Running
* `run <CLUSTERNAME> <REPONAME> <TASKNAME> <CLUSTERNAME> <AWSPROFILE (optional)>`
  * Optional environmental parameters:
    * `URLPATH="<THE PATH>" TESTING=<1> run <CLUSTERNAME> <REPONAME> <TASKNAME> <CLUSTERNAME> <AWSPROFILE (optional)>`
  * Example:
    * `URLPATH=".url" TESTING=1 run <CLUSTERNAME> <REPONAME> <TASKNAME> <CLUSTERNAME> <AWSPROFILE (optional)>`
* If running on Jenkins, I got it working with:
  * `daemonize -E BUILD_ID=dontKillMe <PATHTOFILE>/run.sh <CLUSTERNAME> <REPONAME> <TASKNAME> <CLUSTERNAME> <AWSPROFILE (optional)>`
  * `https://wiki.jenkins.io/display/JENKINS/Spawning+processes+from+build`
