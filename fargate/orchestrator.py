import datetime
import json
import subprocess
import sys
import time

path = "./"

try:
    try:
        if sys.argv[1] == 'testing':
            with open(path +"testingData") as file:
                print file.read()
            sys.exit()
        clusterName = sys.argv[1]
        regionName = sys.argv[2]
    except Exception as e:
        print "Please provide all parameters: cluster name and region"
        sys.exit(1)
    services = subprocess.check_output([path +"getAllTasks", clusterName, regionName])
    returnServices = []
    for service in json.loads(services):
        returnService = {}
        returnService['name'] = service
        runningTasks = subprocess.check_output([path +"getRunningTasks", clusterName, service, regionName])
        returnTasks = []
        for runningTask in json.loads(runningTasks):
            returnTask = {}
            returnTask['name'] = runningTask
            taskDetails = subprocess.check_output([path +"getTaskDetails", clusterName, regionName, runningTask])
            runningTime = json.loads(taskDetails)['tasks'][0]['createdAt']
            returnTask['unix'] = runningTime
            returnTask['created'] = datetime.datetime.utcfromtimestamp(runningTime).strftime('%Y-%m-%d %I:%M%p')
            returnTask['runningTime'] = time.time() - runningTime
            networkInterface = json.loads(taskDetails)['tasks'][0]['attachments'][0]['details'][1]['value']
            returnTask['publicIp'] = subprocess.check_output([path +'getPublicIp', networkInterface]).strip()
            returnTasks.append(returnTask)
        returnService['desiredCount'] = subprocess.check_output([path+'getServiceDesiredCount', clusterName, regionName, service]).strip()
        returnService['tasks'] = returnTasks
        returnServices.append(returnService)
    print json.dumps(returnServices)

except Exception as e:
    print "Exception occurred:"
    print e
    sys.exit(1)
